# Customization 

If you want to add any modifications or changes to this program, it can be done without 
much of a learning curve (hopefully). Here is some more information about how you can customize 
Infinity Search Local.

## User Interface 
Our front-end is built off of the [Bulma CSS Framework](https://bulma.io). Their documentation
is very easy to read and get started with. Once you understand how Bulma works, it should be 
very easy to make design changes. Normal HTML and CSS modifications will also work. 

## Adding Search Engines 
The default search capabilities of this system includes Bing for the web and 
image results and Invidious for the videos. If you wanted to add results from other engines, 
it is a relatively quick process. The way that we get the results and parse is largely 
taken from the [Searx project](https://github.com/asciimoo/searx). They have several more options of 
sites to scrape from [here](https://github.com/asciimoo/searx/tree/master/searx/engines). Since this system only
takes the Bing and Bing Images from them, we made the [Bing.py](/SearchEngines/Bing/Bing.py) and [BingImages.py](/SearchEngines/Bing/BingImages.py)
files standalone by adding some of the utility functions of searx directly into those files. This works nicely 
for quick testing but if you were to add several more search engines into your system, you 
would be better integrating some of their utility files into this repository (We may add this 
in the future). 

The SearX method is not the only way to get results, as the [InstantExtracts.py](/SearchEngines/WikiMedia/InstantExtracts.py) and
[WikiSearches.py](/SearchEngines/WikiMedia/WikiSearches.py) files get results from the Wikimedia public APIs.

So, as long as you have an API or a way of scraping the results that you want to display, you can add it to Infinty Search. 

Once you have your formatted results, all you need to do is make a route for making queries in viewing them 
in the [PublicAPI](/WebsiteAPI/PublicAPI.py) file, making the interface for it in the [results directory](/templates/results) 
and optionally add your own custom sidebar in the [ExternalLinks.py](/MainApplication/ExternalLinks.py) file. You can follow the 
same process as the currently integrated search results to implement your own. 

## Adding Instant Integrations 
For certain search queries, there will be custom integrations built into
the results page. For example, the query "aapl stock" will have a 
TradingView widget built into the page and "3+2" will pull up a calculator. 

If you wanted to add your own integration, there are three main areas
that you need to modify the code.

1. In the [QueryAnalyzer.py](/MainApplication/QueryAnalyzer.py) file, you will need to 
add the words or direct query match that is needed to activate the integration. 

2. Then, you will need to account for this in the [PublicAPI](/WebsiteAPI/PublicAPI.py) file
    by going to the get_results function and add the if statement for checking if the query matched the
    integration e.g: 
    ```python
    if integrations[0] == 'calculator':
        calculation = [[]]
    ```
    
    and then pass the required information into the return render_template function 
    of this function. 

3. Then, go to the [results_integrations.html](/templates/results/results_integrations.html) file
    and add the Jinja if statement or for loop to check to add the integration 
    in if the data was passed in e.g:
    
    ```html
    {% for space in html_space %}
        <div class="column">
            <div class="card">
                <div class="card-content">
                    <div class="content">
                        <span style="color: black"> <b>Space character in HTML:</b> {{space}} </span>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    {% endfor %}
    ```

This process can be repeated for any integration of your choice. 

