# Infinity Search Design
This is just a quick summary of how Infinity Search works. 

For more information about how to customize this, go to the [Customization.md](/Docs/Customization.md) file.

## Getting Results
As long as you can get results from an API or by scraping you can display them in Infinity 
Search. To view some examples of how to do this, go to the [SearchEngines Directory](/SearchEngines) and 
view those files. You can also view some [SearX examples](https://github.com/asciimoo/searx/tree/master/searx/engines) of 
how they do it. 

## Displaying Results
Once you have the results, you can format them how you want to and pass them into the
[results.html](/templates/results/results.html) file use Jinja to iterate through them and 
place them on the page. 

## User Interface
The front-end is built with the [Bulma CSS framework](https://bulma.io/). 

