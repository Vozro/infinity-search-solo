import requests
from bs4 import BeautifulSoup

def get_video_results(query):
    try:
        query = query.replace(' ', '+')
        formatted_results = []

        results = requests.get('https://www.invidio.us/search?q=' + query, timeout=2.5).text

        soup = BeautifulSoup(str(results), 'html.parser')
        search_results = soup.find_all('div', 'pure-u-1 pure-u-md-1-4')

        for video in search_results:
            try:
                video_link = 'https://www.invidio.us' + video.find('a').get('href')
                video_thumbnail = 'https://www.invidio.us' + video.find('img', 'thumbnail').get('src')
                video_length = video.find('p', 'length').text
                video_title = video.find_all('p')[1]
                video_title = video_title.find('a').text

                # formatted_results.append(
                    # {'link': video_link, 'title': video_title, 'thumbnail': video_thumbnail, 'length': video_length})
                formatted_results.append([str(video_link), str(video_title), str(video_thumbnail), str(video_length)])

            except Exception:
                x = 0

        # print(formatted_results)
        return formatted_results

    except Exception:
        return [[]]


if __name__ == '__main__':
    get_video_results('Code')
