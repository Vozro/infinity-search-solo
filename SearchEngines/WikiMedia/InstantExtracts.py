import requests

def get_answers(query):
    try:
        results = requests.get('https://en.wikipedia.org/api/rest_v1/page/summary/' + query, timeout=3).json()

        information = {}
        information['description'] = results['description']
        information['title'] = results['displaytitle']
        information['extract'] = results['extract']
        information['thumbnail_url'] = results['thumbnail']['source']
        information['page_url'] = results['content_urls']['desktop']['page']

        return [information]
    except Exception:
        return []


if __name__ == '__main__':
    import pprint
    pprint.pprint(get_answers('Napolean'))
