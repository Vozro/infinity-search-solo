from flask import Blueprint, render_template, request, redirect
import MainApplication.ExternalLinks as Externals
from exchange_dict import exchange_dict
import MainApplication.QueryAnalyzer as QueryAnalyzer
import SearchEngines.Invidious.Invidious as Invidious
import SearchEngines.WikiMedia.InstantExtracts as InstantExtracts
import SearchEngines.Bing.Bing as Bing
import SearchEngines.Bing.BingImages as BingImages

publicAPI = Blueprint('publicAPI', __name__)


@publicAPI.route('/')
def render_static():
    return render_template('pages/home.html')


@publicAPI.route('/about')
def render_about():
    return render_template('pages/about.html')


@publicAPI.route('/contact')
def render_contact():
    return render_template('pages/contact.html')


@publicAPI.route('/privacy')
def render_privacy():
    return render_template('pages/privacy.html')


def get_results(query, page_number=1):
    words = str(query).split()
    words_in_search = len(words)

    if words_in_search == 0:
        return redirect('/')

    instant_extracts = []
    if page_number == 1:
        instant_extracts = InstantExtracts.get_answers(query)

    try:
        results = Bing.get_results(query, page_number)
    except Exception:
        results = []

    external_links = Externals.get_external_links(query)

    stock_searched = False
    stock = ''

    symbol = ''
    url = ''

    stock_news = False
    crypto_news = False

    calculation = []

    html_space = []

    try:
        integrations = QueryAnalyzer.analyze_query_v2(query)
    except Exception:
        integrations = '', ''

    if integrations[0] == 'calculator':
        calculation = [[]]

    elif integrations[0] == 'stock':
        stock_searched = True
        stock = integrations[1].upper()
        try:
            exchange = exchange_dict[stock]

        except Exception:
            exchange = 'NASDAQ'

        symbol = exchange.upper() + ':' + stock
        url = 'https://www.tradingview.com/symbols/' + exchange + '-' + stock + '/'

    elif integrations[0] == 'crypto':
        crypto_news = True

    elif integrations[0] == 'stock news':
        stock_news = True

    elif integrations[0] == 'html space':
        html_space = ['&nbsp;']

    return render_template('results/results.html', query=query, stock=stock, stock_searched=stock_searched,
                           symbol=symbol, url=url, stock_news=stock_news, crypto_news=crypto_news,
                           instant_extracts=instant_extracts, bing_results=results,
                           external_results=external_links, current='web',
                           calculator=calculation, page_number=page_number, html_space=html_space)


@publicAPI.route('/results', methods=['GET', 'POST'])
def render_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

        except Exception:
            return redirect('/')

        return get_results(query)

    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_results(query, page_number)

    except Exception:
        return redirect('/')

    else:
        return redirect('/')


def get_image_results(query, page_number=1):
    words = query
    words = str(words).split()

    if len(words) == 0:
        return redirect('/')

    external_links = Externals.get_image_links(query)

    try:
        results = BingImages.get_results(query, page_number)
    except Exception:
        results = []

    return render_template('results/results.html', query=query, image_results=results,
                           external_results=external_links, current='images', page_number=page_number)


@publicAPI.route('/results/images', methods=['GET', 'POST'])
def render_image_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']

        except Exception:
            return redirect('/')

        return get_image_results(query)

    try:
        page_number = 1
        if request.args.get('page_number'):
            page_number = int(request.args.get('page_number'))

        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_image_results(query, page_number=page_number)

    except Exception:
        return redirect('/')

    return redirect('/')


# Video  Results ------------------------
def get_video_results(query):

    try:
        results = Invidious.get_video_results(query)
    except Exception:
        results = []

    external_links = Externals.get_video_links(query)

    return render_template('results/results.html', query=query, video_results=results,
                           external_results=external_links, current='videos')


@publicAPI.route('/results/videos', methods=['GET', 'POST'])
def render_video_results():
    if request.method == 'POST':
        try:  # In case someone tried to change the value of the form name
            form_results = dict(request.form)
            query = form_results['Search']
            return get_video_results(query)

        except Exception as e:
            print(e)
            return redirect('/')

    try:
        if request.args.get('q') is not None:
            query = request.args.get('q')
            return get_video_results(query)

    except Exception as e:
        print(e)
        return redirect('/')

    return redirect('/')
