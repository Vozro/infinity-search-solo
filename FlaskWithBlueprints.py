from flask import Flask, render_template, send_from_directory
import os

from WebsiteAPI.PublicAPI import publicAPI

app = Flask(__name__)
app.register_blueprint(publicAPI)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('pages/404.html'), 404


@app.errorhandler(500)
def internal_error(e):
    return render_template('pages/500.html'), 500


if __name__ == '__main__':
    app.run()
