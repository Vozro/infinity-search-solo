# Links that go on the sidebar
def get_external_links(query):
    query = query.replace(' ', '+')

    links = [
        # Open Resources:
        ['Internet Archive Results', 'https://archive.org/search.php?query=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/archive.ico'],
        ['Wikipedia Results', 'https://en.wikipedia.org/wiki/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikipedia.ico'],
        ['Youtube Results', 'https://www.youtube.com/results?search_query=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/youtube.ico'],

        ['GitHub Results', 'https://github.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/github.ico'],
        ['GitLab Results', 'https://gitlab.com/search?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/gitlab.png'],

        ['DuckDuckGo Results', 'https://duckduckgo.com/?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/duckduckgo.ico'],

        # Popular Websites
        ['Unsplash Image Results', 'https://unsplash.com/s/photos/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/unsplash.png'],
        ['Wikimedia Commons Results', 'https://commons.wikimedia.org/wiki/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikimedia.ico'],

        ['Amazon Results', 'https://www.amazon.com/s?k=' + query + '&tag=infinitysea0b-20',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/amazon.ico'],

        ['Invidious Results', 'https://www.invidio.us/search?q=' + query, 'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/invidious.ico'],
        # Forum Searches

        ['Twitter Results', 'https://twitter.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/twitter.ico'],
        ['Reddit Results', 'https://www.reddit.com/search/?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/reddit.ico'],
        ['BoardReader Results', 'https://boardreader.com/s/' + query + '.html',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/boardreader.ico'],
        # Technical Searches
        ['Petey Vid Results', 'https://www.peteyvid.com/index.php?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/peteyvid.ico'],
        ['Stack Overflow Results', 'https://stackoverflow.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/stackoverflow.ico'],
        ['Wolfram Alpha Results', 'https://www.wolframalpha.com/input/?i=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wolfram.ico'],
        # Archive Searches
        ['Project Gutenburg Results', 'https://www.gutenberg.org/ebooks/search/?query=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/gutenberg.ico'],
        ['Wayback Machine Results', 'https://web.archive.org/web/*/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/archive.ico'],
        # ['SlideShare Results', 'https://www.slideshare.net/search/slideshow?searchfrom=header&q=' + query, 'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/slideshare.ico'],
        # Other Search Engines
        ['Mojeek Results', 'https://www.mojeek.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/mojeek.ico'],
        ['Yandex Results', 'https://yandex.com/search/?text=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/yandex.ico'],
        ['Qwant Results', 'https://www.qwant.com/?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/qwant.ico'],
        ['Cliqz Results', 'https://beta.cliqz.com/search?q=' + query + '#channel=infinitysearch',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/cliqz.ico'],
    ]

    return links


def get_image_links(query):
    query = query.replace(' ', '+')
    links = [
        # Image engines
        ['DuckDuckGo Image Results', 'https://duckduckgo.com/?iar=images&iax=images&ia=images&q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/duckduckgo.ico'],
        ['Bing Image Results', 'https://www.bing.com/images/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/bing.ico'],
        ['Google Image Results', 'https://www.google.com/search?hl=en&tbm=isch&source=hp&q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/google.ico'],
        ['Pixabay Results', 'https://pixabay.com/images/search/' + query + '/',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/pixabay.ico'],
        ['Unsplash Results', 'https://unsplash.com/s/photos/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/unsplash.png'],
        ['Flickr Results', 'https://www.flickr.com/search/?text=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/flickr.ico'],
        ['Pexels Results', 'https://www.pexels.com/search/' + query + '/',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/pexels.ico'],
        ['Creative Commons Results', 'https://search.creativecommons.org/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/creativecommons.ico'],
        ['Wikimedia Commons Results', 'https://commons.wikimedia.org/wiki/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikimedia.ico'],
        ['Pinterest Results', 'https://www.pinterest.com/search/pins/?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/pinterest.ico'],
        ['Internet Archive Image Results', 'https://archive.org/details/image?and[]=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/archive.ico'],
        ['Picsearch Image Results', 'https://www.picsearch.com/index.cgi?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/picsearch.ico'],
        ['Nasa Image Results',
         'https://images.nasa.gov/search-results?yearStart=1920&yearEnd=2020&media=image&q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/nasa.ico'],
        ['Tineye Reverse Image Search', 'https://tineye.com/',
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/tineye.ico'],
    ]

    return links


def get_video_links(query):
    query = query.replace(' ', '+')
    links = [
        ['Invidious Search Results', 'https://www.invidio.us/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/invidious.ico'],
        ['Youtube Results', 'https://www.youtube.com/results?search_query=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/youtube.ico'],
        ['Vimeo Results', 'https://vimeo.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/vimeo.png'],
        ['Daily Motion Results', 'https://www.dailymotion.com/search/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/dailymotion.ico'],
        ['Internet Archive Movie Results', 'https://archive.org/details/movies?and[]=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/archive.ico'],
        ['DTube Results', 'https://d.tube/#!/s/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/dtube.ico'],
        ['Bitchute Results',
         'https://search.bitchute.com/renderer?use=bitchute-json&name=Search&login=bcadmin&key=7ea2d72b62aa4f762cc5a348ef6642b8&query=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/bitchute.ico'],
        ['Petey Vid Search Results', 'https://www.peteyvid.com/index.php?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/peteyvid.ico'],
        ['Twitch Results', 'https://www.twitch.tv/search?term=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/twitch.ico'],
        ['LiveLeak Results', 'https://www.liveleak.com/browse?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/liveleak.ico'],
        ['Ted Talk Results', 'https://www.ted.com/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/ted.ico'],
        ['Veoh Results', 'https://www.veoh.com/find/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/veoh.ico'],
        ['Metacafe Results', 'https://www.metacafe.com/videos_about/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/metacafe.ico'],
        ['Vlare Results', 'https://vlare.tv/search/' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/vlare.ico'],
        ['Lbry Results', 'https://lbry.tv/$/search?q=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/lbry.ico'],
    ]
    return links


def get_wiki_links(query):
    query = query.replace(' ', '+')

    links = [
        ['Wikipedia', 'https://wikipedia.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikipedia.ico', ],
        ['Wikibooks', 'https://wikibooks.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikibooks.ico'],
        ['Wikiquote', 'https://wikiquote.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikiquote.ico'],
        ['Wikivoyage', 'https://wikivoyage.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikivoyage.ico'],
        ['Wikisource', 'https://wikivoyage.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikisource.ico'],
        ['Wikimedia Commons', 'https://commons.wikimedia.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikimedia.ico'],
        ['Wikinews', 'https://wikinews.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikinews.ico'],
        ['Wikiversity', 'https://wikiversity.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikiversity.ico'],
        ['Wikidata', 'https://www.wikidata.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikidata.ico'],
        ['Wikimedia Meta', 'https://meta.wikimedia.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikimedia.ico'],
        ['Wiktionary', 'https://wiktionary.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wiktionary.ico'],
        ['Wikispecies', 'https://species.wikimedia.org/w/index.php?search=' + query,
         'https://infinity-search-saved-favicons.s3.amazonaws.com/external_link_favicons/wikispecies.ico'],
    ]

    return links
