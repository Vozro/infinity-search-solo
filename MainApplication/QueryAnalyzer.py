search_topics = {
    # Calculator
    'calculator': 'calculator',

    # Tradingview
    'stock' : 'stock',
    'crypto' : 'crypto',
    'stock news' : 'stock news',
    'investing': 'investing',  # Tradingview widget

}


def topic_search_v2(words):
    searched_for = ''
    word_length = len(words)

    if words[0].lower() in search_topics:
        if word_length == 1:
            return search_topics[words[0].lower()], searched_for

        for word in words[1:]:
            searched_for += word + ' '
        return search_topics[words[0].lower()], searched_for

    if words[word_length - 1].lower() in search_topics:
        if word_length == 1:
            return search_topics[words[word_length - 1].lower()], searched_for

        for word in words[0:word_length - 1]:
            searched_for += word + ' '
        return search_topics[words[word_length - 1].lower()], searched_for

    return '', ''


def calculation_search(query):
    # words = words.split()
    words = query.replace(' ', '')
    calculation = ''
    for word in words:
        is_number = False
        is_math_operation = False
        try:
            num = float(word)
            is_number = True
            calculation += str(num)
        except Exception as e:
            is_number = False

        if word == '(' or word == ')' or word == '^' or word == '*' or word == 'x' or word == '/' or word == '+' or word == '-':
            is_math_operation = True
            calculation += str(word)

        if is_number is False and is_math_operation is False:
            return False, ''

    return True, calculation


def analyze_query_v2(query):
    words = str(query).split()

    if len(words) == 1:
        if words[0].lower() == 'crypto':
            return 'crypto', ''

    if len(words) == 2:
        if words[1].lower() == 'stock':
            return 'stock', words[0]

        if words[0].lower() == 'stock' and words[1].lower() == 'news':
            return 'stock news', ''

        if (words[0].lower() == 'html' and words[1].lower() == 'space') or (words[0].lower() == 'space' and words[1].lower() == 'html'):
            return 'html space', ''

    topic = topic_search_v2(words)

    if topic[0] != '':
        return topic

    calculation = calculation_search(query)
    if calculation[0] is True:
        return "calculator", calculation[1]

    return None, None


if __name__ == '__main__':
    analyze_query_v2('(4+3)/3')

